# Aromat
# by Eresse <eresse@eresse.net>

# Aromat Module
module Aromat

	# Dclone Module
	module Dclone

		# Base Clone:
		# Clone the object if possible.
		# @param [Object] e Any object
		# @return [Object] A clone of e
		def self.base_clone e
			e.clone rescue e
		end
	end
end

# Monkey-patch Array Class
class Array

	# Deep-Clone:
	# Recursively clones every level of the Array.
	# @return [Array] A copy of the original array where each element has been clone'd
	def dclone
		collect { |a| a.respond_to?(:dclone) ? a.dclone : Aromat::Dclone.base_clone(a) }
	end
end

# Monkey-patch Hash Class
class Hash

	# Deep-Clone:
	# Recursively clones every level of the Hash.
	# @return [Hash] A copy of the original hash where each element has been clone'd
	def dclone
		Hash[*(inject([]) { |a, e| a + e }.collect { |e| e.respond_to?(:dclone) ? e.dclone : Aromat::Dclone.base_clone(e) })]
	end
end
