# Aromat
# by Eresse <eresse@eresse.net>

# Monkey-patch Object Class
class Object

	# Try:
	# Call a method if possible, do nothing otherwise.
	# @param [Symbol] meth Method name
	# @param [Array] args Method arguments
	# @param [Object] block Method block
	def try meth, *args, &block
		return nil unless respond_to? meth
		send meth.to_sym, *args, &block
	end
end
