# Aromat
# by Eresse <eresse@eresse.net>

# Monkey-patch Module Class
class Module

	# Module Name:
	# Returns the name of the Module, without its hierarchical tree.
	# @return [String] The name of the Module
	def mod_name
		name[/[^:]+$/]
	end

	# Parent Name:
	# Returns the Module's immediate parent's name.
	def mod_parent_name
		name.index('::') ? name.gsub(/::#{mod_name}$/, '') : ''
	end

	# Parent:
	# Returns the Module's immediate parent.
	def mod_parent
		n = mod_parent_name
		(n == '') ? nil : const_get(n)
	end
end
