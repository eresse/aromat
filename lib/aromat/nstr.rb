# Aromat
# by Eresse <eresse@eresse.net>

# Monkey-patch String Class
class String

	# Non-Empty String:
	# Eliminate Empty Strings.
	# @return [String] The string if non-empty, nil otherwise
	def nstr
		self == '' ? nil : self
	end
end
