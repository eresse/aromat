# Aromat
# by Eresse <eresse@eresse.net>

# Monkey-patch Hash Class
class Hash

	# Deep-Merge:
	# Recursively merges every level of the Hash.
	# @param [Hash] a Hash to merge with
	# @return [Hash] A deep combination of this and a.
	def dmerge a
        m = proc { |k, v1, v2| Hash === v1 && Hash === v2 ? v1.merge(v2, &m) : Array === v1 && Array === v2 ? v1 | v2 : [:undefined, nil, :nil].include?(v2) ? v1 : v2 }
        self.merge(a.to_h, &m)
	end
end
