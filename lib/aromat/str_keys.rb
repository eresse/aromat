# Aromat
# by Eresse <eresse@eresse.net>

# Monkey-patch Array Class
class Array

	# Stringize Keys:
	# Recursively stringizes hash keys.
	# @return [Array] A copy of the original array where each element has had its keys recursively stringized
	def str_keys
		collect { |e| e.respond_to?(:str_keys) ? e.str_keys : e }
	end
end

# Monkey-patch Hash Class
class Hash

	# Stringize Keys:
	# Recursively stringizes hash keys.
	# @return [Hash] A copy of the original hash where each element has had its keys recursively stringized
	def str_keys
		Hash[*(inject([]) { |a, e| (a << (e[0].is_a?(Symbol) ? e[0].to_s : e[0])) << (e[1].respond_to?(:str_keys) ? e[1].str_keys : e[1]) })]
	end
end
