# Aromat
# by Eresse <eresse@eresse.net>

# Monkey-patch String Class
class String

	# Camel Case:
	# Converts String to CamelCase.
	# @return [String] The string converted to camel-case
	def camelcase
		self.gsub(/(^|[_-])([a-zA-Z])/) { |s| s[/[a-zA-Z]$/].upcase }
	end

	# Kebab Case:
	# Converts String to kebab-case.
	# @return [String] The string converted to kebab-case
	def kebabcase
		flatcase '-'
	end

	# Snake Case:
	# Converts String to snake_case.
	def snakecase
		flatcase '_'
	end

	# Flat Case:
	# Generic String converter for kebab-case / snake_case.
	# @param [String] delim Delimiter to be placed between parts
	# @return [String] The string converted to flatcase.
	def flatcase delim = ''
		self.gsub(/((^|[a-z])[A-Z])|([_-][a-zA-Z])/) { |s| (s.length > 1 ? "#{s[/^[a-z]/]}#{delim}" : '') + s[/[a-zA-Z]$/].downcase }
	end
end
