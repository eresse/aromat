# Aromat
# by Eresse <eresse@eresse.net>

# Monkey-patch Array Class
class Array

	# Symbolize Keys:
	# Recursively symbolizes hash keys.
	# @return [Array] A copy of the original array where each element has had its keys recursively symbolized
	def sym_keys
		collect { |e| e.respond_to?(:sym_keys) ? e.sym_keys : e }
	end
end

# Monkey-patch Hash Class
class Hash

	# Symbolize Keys:
	# Recursively symbolizes hash keys.
	# @return [Hash] A copy of the original hash where each element has had its keys recursively symbolized
	def sym_keys
		Hash[*(inject([]) { |a, e| (a << (e[0].respond_to?(:to_sym) ? e[0].to_sym : e[0])) << (e[1].respond_to?(:sym_keys) ? e[1].sym_keys : e[1]) })]
	end
end
