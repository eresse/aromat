# Aromat
# by Eresse <eresse@eresse.net>

# Monkey-patch Date Class
class Date

	# Last X:
	# Get last occurence of a given weekday.
	# @return [Date]
	def wlast wd
		d = self - 1
		d = d - 1 until d.send "#{wd}?".to_sym
		d
	end

	# Next X:
	# Get next occurence of a given weekday.
	# @return [Date]
	def wnext wd
		d = self + 1
		d = d + 1 until d.send "#{wd}?".to_sym
		d
	end
end
