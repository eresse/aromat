# Aromat
# by Eresse <eresse@eresse.net>

# Monkey-patch String Class
class String

	# Right-Pad:
	# Right-Pads a String to a given length using a given filler.
	# @param [Fixnum] size
	# @param [String] fill
	# @return [String] A copy of the original String, padded to [size] with [fill]
	def rpad size, fill = ' '
		s = clone
		s = "#{s}#{fill}" while s.size < size
		s.slice 0, size
	end

	# Left-Pad:
	# Left-Pads a String to a given length using a given filler.
	# @param [Fixnum] size
	# @param [String] fill
	# @return [String] A copy of the original String, padded to [size] with [fill]
	def lpad size, fill = ' '
		s = clone
		s = "#{fill}#{s}" while s.size < size
		s.slice s.size - size, size
	end
end
