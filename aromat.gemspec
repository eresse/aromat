# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'aromat/version'

Gem::Specification.new do |spec|
	spec.name = 'aromat'
	spec.version = Aromat::VERSION
	spec.authors = ['Eresse']
	spec.email = ['eresse@eresse.net']

	spec.summary = 'Small extensions to Ruby core libraries to make your life easier'
	spec.description = 'Provides a few extensions to the Ruby core libraries'
	spec.homepage = 'http://redmine.eresse.net/projects/aromat'
	spec.license = 'MIT'

	spec.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
	spec.require_paths = ['lib']

	spec.add_development_dependency 'bundler'
	spec.add_development_dependency 'rake'
	spec.add_runtime_dependency 'minitest'
end
