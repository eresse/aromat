require 'minitest/autorun'
require 'aromat/try'

class Aromat_TryTest < Minitest::Test

	# Test Try
	def test_try
		assert_nil 'foobar'.try :foo
		assert_equal '32', 32.try(:to_s)
		assert_nil 32.try(:bork)
		assert_equal 35, 32.try(:+, 3)
		assert_equal ['-foo-', '-bar-'], ['foo', 'bar'].try(:collect) { |e| "-#{e}-" }
	end
end
