require 'minitest/autorun'
require 'aromat/try'
require 'aromat/case'

class Aromat_CaseTest < Minitest::Test

	# Test Case
	def test_case
		t = [

			# CamelCase
			{
				in: 'foo',
				out: 'Foo',
				meth: :camelcase
			},
			{
				in: 'foo-bar',
				out: 'FooBar',
				meth: :camelcase
			},
			{
				in: 'foo_bar',
				out: 'FooBar',
				meth: :camelcase
			},
			{
				in: '',
				out: '',
				meth: :camelcase
			},
			{
				in: '-',
				out: '-',
				meth: :camelcase
			},
			{
				in: '_',
				out: '_',
				meth: :camelcase
			},
			{
				in: 'fooBar',
				out: 'FooBar',
				meth: :camelcase
			},

			# kebab-case
			{
				in: 'Foo',
				out: 'foo',
				meth: :kebabcase
			},
			{
				in: 'foo',
				out: 'foo',
				meth: :kebabcase
			},
			{
				in: 'fooBar',
				out: 'foo-bar',
				meth: :kebabcase
			},
			{
				in: 'foo_bar',
				out: 'foo-bar',
				meth: :kebabcase
			},
			{
				in: 'FooBar',
				out: 'foo-bar',
				meth: :kebabcase
			},
			{
				in: '_foo',
				out: '-foo',
				meth: :kebabcase
			},
			{
				in: '-foo',
				out: '-foo',
				meth: :kebabcase
			},
			{
				in: '',
				out: '',
				meth: :kebabcase
			},
			{
				in: '-',
				out: '-',
				meth: :kebabcase
			},
			{
				in: '_',
				out: '_',
				meth: :kebabcase
			},
			{
				in: 'A',
				out: 'a',
				meth: :kebabcase
			},

			# snake_case
			{
				in: 'Foo',
				out: 'foo',
				meth: :snakecase
			},
			{
				in: 'foo',
				out: 'foo',
				meth: :snakecase
			},
			{
				in: 'fooBar',
				out: 'foo_bar',
				meth: :snakecase
			},
			{
				in: 'foo-bar',
				out: 'foo_bar',
				meth: :snakecase
			},
			{
				in: 'FooBar',
				out: 'foo_bar',
				meth: :snakecase
			},
			{
				in: '-foo',
				out: '_foo',
				meth: :snakecase
			},
			{
				in: '_foo',
				out: '_foo',
				meth: :snakecase
			},
			{
				in: '',
				out: '',
				meth: :snakecase
			},
			{
				in: '-',
				out: '-',
				meth: :snakecase
			},
			{
				in: '_',
				out: '_',
				meth: :snakecase
			},
			{
				in: 'A',
				out: 'a',
				meth: :snakecase
			}
		]

		t.each { |tt| assert_equal tt[:out], tt[:in].send(tt[:meth]), "#{tt[:in]} -> #{tt[:out]}" }
	end
end
