require 'minitest/autorun'
require 'date'
require 'aromat/try'
require 'aromat/duration'

class Aromat_DurationTest < Minitest::Test

	# Test Duration
	def test_duration
		assert_equal '3 Years 8 Weeks 6 Days 9 Hours 15 Minutes 8 Seconds', (1490170508 - Date.parse('Jan 23 2014').to_time.to_i).duration
		assert_equal '3 Years 8 Weeks 6 Days 9 Hours 15 Minutes', (1490170500 - Date.parse('Jan 23 2014').to_time.to_i).duration
		assert_equal '3 Years 8 Weeks 6 Days 9 Hours 21 Seconds', (1490169621 - Date.parse('Jan 23 2014').to_time.to_i).duration
		assert_equal '3 Years 8 Weeks 6 Days', (1490170508 - Date.parse('Jan 23 2014').to_time.to_i).duration(3)
		assert_equal '3 Years 8 Weeks', (1490170500 - Date.parse('Jan 23 2014').to_time.to_i).duration(2)
		assert_equal '1 Day', 86400.duration
		assert_equal '2 Days', (86400 * 2).duration
		assert_equal '3 Hours 1 Minute 12 Seconds', ((3600 * 3) + 60 + 12).duration
		assert_equal '3 Hours 1 Minute', ((3600 * 3) + 60 + 12).duration(2)
		assert_equal '3 Hours', ((3600 * 3) + 60 + 12).duration(1)
	end
end
