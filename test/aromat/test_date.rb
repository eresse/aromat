require 'minitest/autorun'
require 'date'
require 'aromat/try'
require 'aromat/date'

class Aromat_DateTest < Minitest::Test

	# Test Last Date
	def test_last_date
		assert_equal Date.parse('2017 Nov 3'), Date.parse('2017-nov-10').wlast(:friday)
	end

	# Test Next Date
	def test_next_date
		assert_equal Date.parse('2017 Nov 17'), Date.parse('2017-nov-10').wnext(:friday)
	end
end
