require 'minitest/autorun'
require 'aromat/try'
require 'aromat/module'

class Aromat_ModuleTest < Minitest::Test

	# Test Module
	module TestModule

		# Test Sub-Module
		module TestSubModule

			# Nested Raccoon
			module NestedRaccoon
			end
		end
	end

	# Test Module Name
	def test_mod_name
		assert_equal 'TestModule', TestModule.mod_name
		assert_equal 'TestSubModule', TestModule::TestSubModule.mod_name
		assert_equal 'NestedRaccoon', TestModule::TestSubModule::NestedRaccoon.mod_name
	end

	# Test Module Parent Name
	def test_mod_parent_name
		assert_equal '', Aromat_ModuleTest.mod_parent_name
		assert_equal 'Aromat_ModuleTest', TestModule.mod_parent_name
		assert_equal 'Aromat_ModuleTest::TestModule', TestModule::TestSubModule.mod_parent_name
		assert_equal 'Aromat_ModuleTest::TestModule::TestSubModule', TestModule::TestSubModule::NestedRaccoon.mod_parent_name
	end

	# Test Module Parent
	def test_mod_parent
		assert_nil Aromat_ModuleTest.mod_parent
		assert_equal Aromat_ModuleTest, TestModule.mod_parent
		assert_equal Aromat_ModuleTest::TestModule, TestModule::TestSubModule.mod_parent
		assert_equal Aromat_ModuleTest::TestModule::TestSubModule, TestModule::TestSubModule::NestedRaccoon.mod_parent
	end
end
