require 'minitest/autorun'
require 'aromat/try'
require 'aromat/nstr'

class Aromat_NStrTest < Minitest::Test

	# Test NStr
	def test_nstr
		assert_nil ''.nstr
		assert_equal 'foo', 'foo'.nstr
		assert_nil nil.try(:nstr)
		assert_equal 'bar', 'bar'.try(:nstr)
	end
end
