require 'minitest/autorun'
require 'aromat/dclone'

class Aromat_DcloneTest < Minitest::Test

	# Test Clone
	def test_clone
		a = { foo: 'bar', mail: true, bar: :kill_me, nested: [{ problems: 'none' }] }
		b = a.dclone
		b[:nested] << :foo
		b[:nested][0][:problems] = 'no way'
		b[:test] = :bork
		b[:foo] = nil
		b.delete :bar
		assert a != b
		assert_equal({ foo: 'bar', mail: true, bar: :kill_me, nested: [{ problems: 'none' }] }, a)
		assert_equal({ foo: nil, mail: true, nested: [{ problems: 'no way' }, :foo], test: :bork }, b)
	end
end
