require 'minitest/autorun'
require 'aromat/str_keys'

class Aromat_StringizeKeysTest < Minitest::Test

	# Test Array
	def test_array
		t = [
			{
				in: [:foo, { test: :zion }, :bar],
				out: [:foo, { 'test' => :zion }, :bar]
			},
			{
				in: [{ foo: 'bar', test: :zion, hello: :world }, 'bla', 'foobar', 'bork'],
				out: [{ 'foo' => 'bar', 'test' => :zion, 'hello' => :world }, 'bla', 'foobar', 'bork']
			},
			{
				in: [:foo, :test, :zion],
				out: [:foo, :test, :zion]
			}
		]
		t.each { |e| assert_equal e[:out], e[:in].str_keys }
	end

	# Test Hash
	def test_hash
		t = [
			{
				in: { 'foo' => :bar },
				out: { 'foo' => :bar }
			},
			{
				in: { bork: :test, foo: 'bar', zion: :blabla, hello: 'world' },
				out: { 'bork' => :test, 'foo' => 'bar', 'zion' => :blabla, 'hello' => 'world' }
			},
			{
				in: { test: :bork, array: [1, 2, 3, 4], hash: { blabla: :foobar } },
				out: { 'test' => :bork, 'array' => [1, 2, 3, 4], 'hash' => { 'blabla' => :foobar } }
			}
		]
		t.each { |e| assert_equal e[:out], e[:in].str_keys }
	end

	# Test Deep
	def test_deep
		t = [
			{
				in: [[{ foo: 'bar', test: 'bla' }, { bork: 'bork' }, 'subitem'], 'item'],
				out: [[{ 'foo' => 'bar', 'test' => 'bla' }, { 'bork' => 'bork' }, 'subitem'], 'item']
			},
			{
				in: { bla: 'bla', bork: ['check_it', { your_recursion: ['is_weak', { and: ['fragile'] }] }], test: { deep: [{ nested: { deeper: 'wow' } }, 'fuuuuu'] }, foo: [{ hello: 'world' }, 'borkbork'], bar: { test: 'foobar' } },
				out: { 'bla' => 'bla', 'bork' => ['check_it', { 'your_recursion' => ['is_weak', { 'and' => ['fragile'] }] }], 'test' => { 'deep' => [{ 'nested' => { 'deeper' => 'wow' } }, 'fuuuuu'] }, 'foo' => [{ 'hello' => 'world' }, 'borkbork'], 'bar' => { 'test' => 'foobar' } }
			}
		]
		t.each { |e| assert_equal e[:out], e[:in].str_keys }
	end

	# Test non-string keys
	def test_non_string_keys
		t = [
			{
				in: { ['foo', 'bar'] => 'test' },
				out: { ['foo', 'bar'] => 'test' }
			},
			{
				in: [{ 4 => 32 }, { { 'test' => 'foo' } => { foo: :bar } }, 'blabla'],
				out: [{ 4 => 32 }, { { 'test' => 'foo' } => { 'foo' => :bar } }, 'blabla']
			}
		]
		t.each { |e| assert_equal e[:out], e[:in].str_keys }
	end
end
