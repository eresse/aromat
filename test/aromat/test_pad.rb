require 'minitest/autorun'
require 'aromat/pad'

class Aromat_PadTest < Minitest::Test

	# Test rpad
	def test_rpad
		t = [
			{ in: ['foo', 10, '0'],
			  out: 'foo0000000'
			},
			{ in: ['foo', 0, '0'],
			  out: ''
			},
			{ in: ['foo', 3, '0'],
			  out: 'foo'
			},
			{ in: ['foo', 10],
			  out: 'foo       '
			},
			{ in: ['foo', 3],
			  out: 'foo'
			},
			{ in: ['foo', 0],
			  out: ''
			},
			{ in: ['foo', 10, '#'],
			  out: 'foo#######'
			}
		]

		t.each { |tt| assert_equal tt[:out], tt[:in].shift.rpad(*(tt[:in])) }
	end

	# Test lpad
	def test_lpad
		t = [
			{ in: ['foo', 10, '0'],
			  out: '0000000foo'
			},
			{ in: ['foo', 0, '0'],
			  out: ''
			},
			{ in: ['foo', 3, '0'],
			  out: 'foo'
			},
			{ in: ['foo', 10],
			  out: '       foo'
			},
			{ in: ['foo', 3],
			  out: 'foo'
			},
			{ in: ['foo', 0],
			  out: ''
			},
			{ in: ['foo', 10, '#'],
			  out: '#######foo'
			}
		]

		t.each { |tt| assert_equal tt[:out], tt[:in].shift.lpad(*(tt[:in])) }
		end
end
