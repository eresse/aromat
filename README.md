# Aromat

Small extensions to Ruby core libraries to make your life easier

## Presentation

This library provides a few extensions to the Ruby core libraries in order to simplify use of some common patterns.

## Installation

### Gemfile
```ruby
gem 'aromat'
```

### Terminal
```bash
gem install -V aromat
```

## Usage

### Symbolize Keys

A _sym_keys_ method is monkey-patched into the *Array* and *Hash* classes, and provides a way to recursively convert *keys* to *symbols*.
This is mostly useful when loading Hashes from external resources (YAML, CSV, JSON, etc...), where keys will be stored as strings.

```ruby
a = { 'foo' => 'bar', 'nested' => [{ 'problems' => 'none' }] }
a.sym_keys
# => {:foo=>"bar", :nested=>[{:problems=>"none"}]}
```

### Stringize Keys

A _str_keys_ method is monkey-patched into the *Array* and *Hash* classes, and provides a way to recursively convert *keys* to *strings*.
This is pretty much the opposite of the 'sym_keys' feature presented just above.

```ruby
a = { foo: 'bar', nested: [{ problems: 'none' }] }
a.sym_keys
# => {"foo"=>"bar", "nested"=>[{"problems"=>"none"}]}
```

### Deep-clone

A _dclone_ method is monkey-patched into the *Array* and *Hash* classes, and provides a way to recursively *deep-clone* an instance.

```ruby
a = { foo: 'bar', mail: true, bar: :kill_me, nested: [{ problems: 'none' }] }
b = a.dclone
b[:nested] << :foo
b[:nested][0][:problems] = 'no way'
b[:test] = :bork
b[:foo] = nil
b.delete :bar
a
# => {:foo=>"bar", :mail=>true, :bar=>:kill_me, :nested=>[{:problems=>"none"}]}
b
# => {:foo=>nil, :mail=>true, :nested=>[{:problems=>"no way"}, :foo], :test=>:bork}
```

### Right/Left Padding

Two padding methods _rpad_ and _lpad_ are monkey-patched into the *String* class, and provide a way to pad any string to a given length using a given filler string.

```ruby
a = 'foo'
a.rpad 10
# => "foo       "
a.rpad 10, '-'
# => "foo-------"
a.rpad 2, '-'
# => "fo"
a.lpad 10
# => "       foo"
a.lpad 10, '-'
# => "-------foo"
a.rpad 2, '-'
# => "oo"
```

### Object Try (Undefined Method Call)

A _try_ method is monkey-patched into the *Object* root class, providing a means to call undefined methods on objects without raising anything (returning *nil* instead).

```ruby
'foobar'.try :foo
# => nil
32.try :to_s
# => '32'
32.try :+, 3
# => 35
['foo', 'bar'].try(:collect) { |e| "-#{e}-" }
# => ['-foo-', '-bar-']
```

### Get Non-Empty String

A _nstr_ method is monkey-patched into the *String* class. This method returns nil when the String is empty, the String otherwise.

```ruby
''.nstr
# => nil
'foo'.nstr
# => 'foo'
nil.try :nstr
# => nil
'bar'.try :nstr
# => 'bar'
```

### String Case Conversions

Case-conversion methods are monkey-patched into the *String* class. The following methods are made available:
* camelcase ('foo-bar' => 'FooBar')
* snakecase ('FooBar' => 'foo_bar')
* kebabcase ('FooBar' => 'foo-bar')

```ruby
'foo_bar'.camelcase
# => 'FooBar'
'foo-bar'.camelcase
# => 'FooBar'

'FooBar'.snakecase
# => 'foo_bar'
'foo-bar'.snakecase
# => 'foo_bar'

'FooBar'.kebabcase
# => 'foo-bar'
'foo_bar'.kebabcase
# => 'foo-bar'
```

### Next / Last weekday

The *Date* class is monkey-patched to include _wnext_ and _wlast_ methods. These return the next/last occurrence of a given weekday.

```ruby
Date.parse('2017 November 10').wnext(:friday).strftime '%Y %B %d'
# => '2017 November 17'
Date.parse('2017 November 10').wlast(:friday).strftime '%Y %B %d'
# => '2017 November 03'
```

### Module utilities

#### Get Module Name

A _mod_name_ method is monkey-patched into the *Module* class. This allows obtaining the name of any module, without its hierarchical tree.

```ruby
module Foo
	module Bar
	end
end

Foo.name
# => 'Foo'

Foo.mod_name
# => 'Foo'

Foo::Bar.name
# => 'Foo::Bar'

Foo::Bar.mod_name # <= This is where it gets interesting
# => 'Bar'
```

#### Get Module Parent Name

A _mod_parent_name_ method is monkey-patched into the *Module* class. This allows obtaining the name of any module's parent.

```ruby
module Foo
	module Bar
		module Test
		end
	end
end

Foo.mod_parent_name
# => ''

Foo::Bar.mod_parent_name
# => 'Foo'

Foo::Bar::Test.mod_parent_name
# => 'Foo::Bar'
```

#### Get Module Parent

A _mod_parent_ method is monkey-patched into the *Module* class. This allows obtaining any module's parent.

```ruby
module Foo
	module Bar
		module Test
		end
	end
end

Foo.mod_parent
# => nil

Foo::Bar.mod_parent
# => Foo

Foo::Bar::Test.mod_parent_name
# => Foo::Bar
```

### Time Duration

A _duration_ method is monkey-patched into the 'Numeric' class. This method returns a textual string representation of the time duration.
An optional *elements* argument allows setting the maximum number of time-elements (days, hours, minutes, ...) to display.

```ruby
86400.duration
# => '1 Day'
((3600 * 3) + 60 + 12).duration
# => '3 Hours 1 Minute 12 Seconds'
((3600 * 3) + 60 + 12).duration 1
# => '3 Hours'
((3600 * 3) + 60 + 12).duration 2
# => '3 Hours 1 Minute'
```

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
